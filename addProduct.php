<?php
  
  include('conectToDB.php');

  if ($conn -> connect_error) {

      die ("no conect" . "<br>" . $conn -> connect_error );
    
    } else {


      $sql = "SELECT * FROM brand ";
      
      $result = $conn->query($sql);

      if ($result -> num_rows > 0 ) {
        
        $catOption = "" ;

        while ($row = $result -> fetch_assoc()) {

          $catOption = $catOption." <option value='". $row['brand_id'] ."'>".$row['brand_name']."</option> " ;

        }
 
      } else {
        die ("no resev any data");
      }
      
    }

  //$conn -> cloase ;

?>


<!DOCTYPE html>
<html lang="en">

<head>
  <title> Add Product's </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>


<body style="background-color: ;">


  <?php include('theNavbarForAll.php'); ?>



  <div class="container" style="margin-top: 60px;">
    <h2>Add a new Product</h2>

    <form action="theNewProduct.php" method="post" enctype="multipart/form-data" autocomplete="off">

      <!-- add a name for the product -->
      <div class="form-group">
        <label for="NP"> Name Of The Product: </label>
          <input type="text" class="form-control" placeholder="Enter the Product name" name="productName" id="NP" required="required">
      </div>

      <!-- add a price for the product -->
      <div class="form-group">
        <label for="PP"> Price Of The Product: </label>
          <input type="text" class="form-control" placeholder="Enter the Product Price" name="productPrice" id="PP" required="required">
      </div>

      <!-- add a Describe for the product -->
      <div class="form-group">
        <label for="PD"> Describe For The Product: </label>
          <textarea class="form-control" placeholder="Enter Describe For The Product" name="productDescribe" id="PD" rows="5"></textarea>
      </div>

      <div class="form-group">
        <label for="PImg"> Picture Of The Product: </label>
          <input type="file" class="form-control" placeholder="Enter the Product Picture" name="file" id="PImg">
      </div>

      <!-- get the Category from the DB -->
      <div class="form-group">
        <label for="option">Select Category:</label>
        <select name="productCategory" class="form-control" id="option">
          <option value=""> select a Category </option>
          
          <?php echo $catOption; ?>
        </select>
      </div>

      <button type="submit" class="btn btn-primary" name="summit">Add The Product</button>


    </form>

  </div>

</body>


</html>