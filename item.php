<?php


  include('conectToDB.php');


  $product_num = $_GET["id"] ;

  if ($conn -> connect_error) {

    die ("no coneect to database " ."<br>" . $conn -> connect_error);

  } else {
    
    

    $sql = "SELECT * FROM product WHERE product_id= '"."$product_num"."' " ;
    $sqlDesc = " SELECT * FROM prodinfo WHERE desID = '"."$product_num"."' "  ;

    $result = $conn->query($sql);
    $resultDesc = $conn->query($sqlDesc);

      if ($result ->num_rows > "0" && $resultDesc > "0" ) {
        
        while ($row = $result -> fetch_assoc()) {
          if ($row['img'] == "null" or "0" ) {
            $row['img'] = 'http://placehold.it/700x400' ;
          }

          $nameOfItem = $row["product_name"] ;
          $priceOfItem = $row["price"] ;
          $imgOfItem = " <img class='card-img-top img-fluid' src='". $row["img"]. "'' alt='".$row["product_name"]."' title='".$row["product_name"]."'> ";
          $describeOfProduct = $row['fastInfo'] ;
   
        }

        $Processor     ="";
        $VideoCard     ="";
        $Memory        ="";
        $HardDrive     ="";
        $Display       ="";
        $OpticalDrive  ="";
        $Wireless      ="";
        $PrimaryBattery="";

        while ($rowDesc = $resultDesc -> fetch_assoc()) {
          
          $Processor      = $rowDesc['Processor'];
          $VideoCard      = $rowDesc['Video Card'];
          $Memory         = $rowDesc['Memory'];
          $HardDrive      = $rowDesc['Hard Drive'];
          $Display        = $rowDesc['Display'];
          $OpticalDrive   = $rowDesc['Optical Drive'];
          $Wireless       = $rowDesc['Wireless'];
          $PrimaryBattery = $rowDesc['Primary Battery'];

   
        }

      } else {
        
        echo "no data :(" ;
      }
      
  }

  //$conn -> cloase ;
  
?>




<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Shop Item - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/shop-item.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <?php include('theNavbarForAll.php'); ?>

  <!-- Page Content -->
  <div class="container">

    <div class="row">


      <div class="col-lg-4">

        <h1 class="my-4"> <?php echo $nameOfItem; ?> </h1>
        <!--  <img class="card-img-top img-fluid" src="http://placehold.it/900x400" alt="">  -->
        
          <?php echo $imgOfItem; ?>
        
      </div>

      
      <!-- /.col-lg-3 -->

      <div class="col-lg-8">

        <div class="card mt-4">
           
          <div class="card-body">
            <h2><?php echo $nameOfItem; ?></h2>
            <h4><?php echo $priceOfItem; ?></h4>
            <p class="card-text"> <?php echo $describeOfProduct; ?> </p>
            <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
            4.0 stars
          </div>
        </div>
        <!-- /.card -->

        <div class="card card-outline-secondary my-4">
          <div class="card-header">
            Product Describe 
          </div>
          <div class="card-body">
            
            <p> <?php echo $Processor; ?> </p>
            <p> <?php echo $VideoCard; ?> </p>
            <p> <?php echo $Memory; ?> </p>
            <p> <?php echo $HardDrive; ?> </p>
            <p> <?php echo $Display; ?> </p>
            <p> <?php echo $OpticalDrive; ?> </p>
            <p> <?php echo $Wireless; ?> </p>
            <p> <?php echo $PrimaryBattery; ?> </p>

          </div>
        </div>
        <!-- /.card -->

      </div>
      <!-- /.col-lg-9 -->

    </div>

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
